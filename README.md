# Krieger

Krieger (_/ˈkʁiːɡɐ/_) is inspired by [Geo-Fi](https://github.com/baskinomics/Geo-Fi), an Android application I developed in graduate school but updated to take advantage of modern cloud and IoT infrastructures. The project name of _Kreiger_ is in reference to [Dr. Krieger](https://en.wikipedia.org/wiki/List_of_Archer_characters#Dr._Krieger) from the television show _Archer_, and a play on the statistical method being applied, i.e. [regression-kriging (RK)](https://en.wikipedia.org/wiki/Regression-kriging). The project avatar is an image of Dr. Krieger with various filters applied to generate an isopleth representation, which will be how the predicted and known wireless signal strength will be displayed on the map to the user.

This is a mutli-project repository that contains:
* Lambda functions (?) for using RK to predict wireless signal strength for a given location. Initially intended to be used with discrete polygonal boundaries, i.e. a bounding box.
* Some sort of client application to display this data in realtime?

## Notes
* AWS Android SDK
* [AWS IoT Greengrase](https://aws.amazon.com/greengrass/) ?
* [AWS Amplify](https://aws.amazon.com/amplify/) ?
* [Making the eventual project avatar of the Doctor's face](https://www.cemetech.net/forum/viewtopic.php?t=2910&start=0)
* [Wiktionary - Krieger](https://en.wiktionary.org/wiki/Krieger)
* [Awesome Geospatial](https://github.com/sacridini/Awesome-Geospatial)